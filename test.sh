./png_transform imgs/kalray.png imgs_transformed/kalray.png
if [ $? -ne 0 ]; then
	echo
	echo "Error png_transform"
	exit
fi
echo
compare -metric rmse imgs_transformed/kalray.png goldens/kalray.png /dev/null
if [ $? -ne 0 ]; then
	echo
	echo "Error compare"
	exit
fi
echo
echo
./png_transform imgs/curious_cat.png imgs_transformed/curious_cat.png
if [ $? -ne 0 ]; then
	echo
	echo "Error png_transform"
	exit
fi
compare -metric rmse imgs_transformed/curious_cat.png goldens/curious_cat.png /dev/null
if [ $? -ne 0 ]; then
	echo
	echo "Error compare"
	exit
fi
echo
echo
./png_transform imgs/manycore-img.png imgs_transformed/manycore-img.png
if [ $? -ne 0 ]; then
	echo
	echo "Error png_transform"
	exit
fi
compare -metric rmse imgs_transformed/manycore-img.png goldens/manycore-img.png /dev/null
if [ $? -ne 0 ]; then
	echo
	echo "Error compare"
	exit
fi
echo
echo
./png_transform imgs/kalray.png imgs_transformed/kalray_Rx1.2_Gx0.8_Bx0.8.png 1.2 0.8 0.8
compare -metric rmse imgs_transformed/kalray_Rx1.2_Gx0.8_Bx0.8.png goldens/kalray_Rx1.2_Gx0.8_Bx0.8.png /dev/null
if [ $? -ne 0 ]; then
	echo
	echo "Error png_transform"
	exit
fi
echo
echo
./png_transform imgs/manycore-img.png imgs_transformed/manycore-img_Rx1.2_Gx0.8_Bx0.8.png 1.2 0.8 0.8
compare -metric rmse imgs_transformed/manycore-img_Rx1.2_Gx0.8_Bx0.8.png goldens/manycore-img_Rx1.2_Gx0.8_Bx0.8.png /dev/null
if [ $? -ne 0 ]; then
	echo
	echo "Error compare"
	exit
fi

echo
echo
echo "End of tests : success"
echo
