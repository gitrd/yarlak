CC=gcc
CFLAGS=-g -W -Wall -pedantic
LDFLAGS=-g -lpng
EXEC=png_transform

all:$(EXEC)

png_transform: png_transform.o
	$(CC)  png_transform.o -o png_transform $(LDFLAGS)

png_transform.o: png_transform.c
	$(CC) $(CFLAGS) png_transform.c -c -o png_transform.o

clean:
	rm -f *.o png_transform

